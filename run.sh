#!/bin/bash
set -x

ping g.co -c 3

dotnet tool install --global dotnet-ef

cd Library
#dotnet restore --configfile ../nuget.config
dotnet restore 
dotnet ef migrations add init
dotnet ef database update
cd ..

cd MailService
ln -s ../Library/mydb.db
#dotnet restore --configfile ../nuget.config
dotnet restore 
dotnet run 
