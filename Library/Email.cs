using System;

namespace MailService.Models
{
    public class Email
    {
        public virtual Guid Id { get; set; }
        public virtual string FromAddress { get; set; }
        public virtual string FromName { get; set; }
        public virtual string ToAddress { get; set; }
        public virtual string ToName { get; set; }
        public virtual string Subject { get; set; }
        public virtual string Body { get; set; }
        
    }
}