using GraphQL.Server;
using GraphQL.Types;
using MailService.Repo;
using MailService.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MailService;
using Microsoft.Extensions.Configuration;

namespace Example
{
    /* Note all servers must use the same address and port because these are pre-registered with the various providers. */
    public class Startup
    {
        public Startup(IConfiguration config)
        {
            Configuration = config;
        }

        public IConfiguration Configuration { get; set; }

        public void ConfigureServices(IServiceCollection services)
        {

            // TODO: remove unneeded types 
            // TODO: OAuth
            services.Configure<KestrelServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });
            services.AddSingleton<EmailService>();
            services.AddSingleton<EmailRepo>();
            services.AddSingleton<IConfigurationService, ConfigurationService>();
            services.AddSingleton<EmailStatusQuery>();
            services.AddSingleton<EmailMutation>();
            services.AddSingleton<StatusViewModelType>();
            services.AddSingleton<EmailInputType>();
            services.AddSingleton<EmailType>();
            services.AddSingleton<ISchema, EmailSchema>();

            services.AddLogging(builder => builder.AddConsole());
            services.AddHttpContextAccessor();
            services.AddAuthentication()
                    .AddGoogle(options =>
            {
                options.ClientId = "1028339744850-bg05hoh3hkfu1unj28h560b141pbuj2p.apps.googleusercontent.com";
                options.ClientSecret = "sKKno_On0cxmwHUITQ6DSiFl";
            });

            services.AddGraphQL(options =>
            {
                options.EnableMetrics = true;
                options.ExposeExceptions = true;
            })
            .AddNewtonsoftJson(); // or use AddSystemTextJson for .NET Core 3+
        }


        public void Configure(IApplicationBuilder app)
        {
            // add http for Schema at default url /graphql
            app.UseGraphQL<ISchema>();
        }
    }
}


