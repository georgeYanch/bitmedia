using GraphQL;
using GraphQL.Types;
using MailService.Models;
using MailService.Services;

namespace MailService
{
    public class EmailMutation : ObjectGraphType
    {
        public EmailMutation(EmailService emailService)
        {
            Field<EmailType>(
                "sendMail",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<EmailInputType>> {Name = "email"}
                ),
                resolve: context =>
                {
                    var arg = context.GetArgument<Email>("email");
                    var id = emailService.Send(arg).GetAwaiter().GetResult().Id;
                    arg.Id = id;
                    return arg;
                });
        }
    }
}

