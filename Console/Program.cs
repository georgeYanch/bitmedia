﻿namespace MailService
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using MailService.Models;
    using MailService.Repo;
    using MailService.Services;

    class Program
    {
        static async Task Main(string[] args)
        {
            List<Guid> ids = new List<Guid>();
            var tasks = new List<Task<SendResult>>();
            var repo = new EmailRepo();
            var emailService = new EmailService(repo, new ConfigurationService());

            for (int i = 0; i < 6; i++)
            {
                Email email = new Email()
                {
                    FromAddress = "bogdan@gmail.com",
                    ToAddress = "bgdn2014@gmail.com",
                    Subject = "Testing",
                    Body = "Hello World!"
                };

                tasks.Add(emailService.Send(email));
            }

            foreach (var task in tasks)
            {
                var id = (await task).Id;
                Console.WriteLine(id);
                ids.Add(id);
            }

            foreach (var id in ids)
            {
                var status = repo.GetStatus(id);
                Console.WriteLine(status);
            }

            await Task.Delay(3000);

            foreach (var id in ids)
            {
                var status = repo.GetStatus(id);
                Console.WriteLine(status);
            }
        }
    }
}
